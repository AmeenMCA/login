import logo from './logo.svg';
import './App.scss';
import photo from './photo.png'
import logoicon from './logo.png'
import googlelogo from './google-logo.png'

function App() {
  return (
    <div className="App">
    <div className="Design-poster">
      
    <img src={logoicon} className='logo-icon'/>
    <h1>All-in-one workspace</h1>
    <h4>Write,Plan,Collabrate,and get organized.Prufer is all you need - in one tool.</h4>
      <img src={photo} />
      <div className='poster-footer'><p>@prufer_product</p><p>LinkedIn</p></div>
    </div>
    <div className="Login-Page">
    <div className="Header">
      <h4>Sign Up</h4>
      </div>
      <div className="Login">
      <h1>Sign In to Prufer</h1>
      <p className="google"><img src={googlelogo} className="google-logo"/>Sign in with Google</p>
      <p>or</p>
      <div className="input-box">
      <input type="text" className="input" placeholder="Email address"/>
      
      <input type="password" className="input" placeholder="Password"/>
      </div>
      <div className="label">
      <div className="reminder"> 
       <input type="checkbox"/>
      <p>Remember me</p></div>
       <p>Forget Password?</p>
      </div>
      <div className='signup'>
      <button>Sign In</button>
      </div>
      
    </div>
    <footer>
      This site is Protected by reCAPTCHA and Google <span>Privacy Policy</span> and <span>Terms of Services</span>  apply.
    </footer>
    </div>
    
   </div>
 
    );
}

export default App;
